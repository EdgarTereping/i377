package model;

public class Report {

    private long count;
    private long averageOrderAmount;
    private long turnoverWithoutVAT;
    private double turnoverVAT;
    private double turnoverWithVAT;

    public Report(long count, long averageOrderAmount, long turnoverWithoutVAT, double turnoverVAT, double turnoverWithVAT) {
        this.count = count;
        this.averageOrderAmount = averageOrderAmount;
        this.turnoverWithoutVAT = turnoverWithoutVAT;
        this.turnoverVAT = turnoverVAT;
        this.turnoverWithVAT = turnoverWithVAT;
    }

    public long getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public long getAverageOrderAmount() {
        return averageOrderAmount;
    }

    public void setAverageOrderAmount(Integer averageOrderAmount) {
        this.averageOrderAmount = averageOrderAmount;
    }

    public long getTurnoverWithoutVAT() {
        return turnoverWithoutVAT;
    }

    public void setTurnoverWithoutVAT(Integer turnoverWithoutVAT) {
        this.turnoverWithoutVAT = turnoverWithoutVAT;
    }

    public double getTurnoverVAT() {
        return turnoverVAT;
    }

    public void setTurnoverVAT(Integer turnoverVAT) {
        this.turnoverVAT = turnoverVAT;
    }

    public double getTurnoverWithVAT() {
        return turnoverWithVAT;
    }

    public void setTurnoverWithVAT(Integer turnoverWithVAT) {
        this.turnoverWithVAT = turnoverWithVAT;
    }
}
