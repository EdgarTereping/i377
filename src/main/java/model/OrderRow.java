package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown=true)
@Embeddable
@Data
public class OrderRow {

    @Column(name = "item_name")
    private String itemName;

    @NotNull
    @Min(value = 1, message = "Quantity must be greater than 0")
    private Integer quantity;

    @NotNull
    @Min(value = 1, message = "Price must be greater than 0")
    private Integer price;
}
