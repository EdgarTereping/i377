package controller;

import dao.OrderDao;
import model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderDao dao;

    @GetMapping("orders")
    public List<Order> getOrders(){
        return dao.getOrderList();
    }

    @GetMapping("orders/{orderId}")
    public Order getOrderById(@PathVariable("orderId") Long orderId) {
        return dao.getOrderById(orderId);
    }

    @PostMapping("orders")
    public Order insertOrder(@RequestBody @Valid Order order) {
        return dao.insertOrder(order);
    }

    @DeleteMapping("orders/{orderId}")
    public void deleteOrder(@PathVariable("orderId") Long orderId) {
        dao.deleteOrder(orderId);
    }

    @DeleteMapping("orders")
    public void deleteAllOrders() {
        dao.deleteAllOrders();
    }

}