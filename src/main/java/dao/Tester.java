package dao;

import conf.DbConfig;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Tester {

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext ctx =
              new AnnotationConfigApplicationContext(DbConfig.class);

        OrderDao dao = ctx.getBean(OrderDao.class);

//        dao.insertOrder(new Order("123"));

        System.out.println(dao.getOrderList());

//        dao.delete(2L);
//        System.out.println(dao.findById(1L));

        ctx.close();
    }
}