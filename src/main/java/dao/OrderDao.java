package dao;

import model.Order;
import model.OrderRow;

import java.util.List;

public interface OrderDao {

    Order insertOrder(Order order);

    Order getOrderById(Long id);

    List<Order> getOrderList();

    OrderRow insertOrderRow (OrderRow orderrow);

    void deleteOrder(Long id);

    void deleteAllOrders();

}
