package dao;

import model.Order;
import model.OrderRow;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Primary
@Repository
public class OrderHsqlDao implements OrderDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public Order insertOrder(Order order) {
        if (order.getId() == null){
            em.persist(order);
        } else {
            em.merge(order);
        }

        return order;
    }

    @Override
    public Order getOrderById(Long id) {
        TypedQuery<Order> query = em.createQuery("select o from Order o where o.id = :id", Order.class);

        query.setParameter("id", id);

        return query.getSingleResult();
    }

    @Override
    public List<Order> getOrderList() {
        return em.createQuery("select o from Order o", Order.class).getResultList();
    }

    @Override
    public OrderRow insertOrderRow(OrderRow orderRow) {
        em.persist(orderRow);

        return orderRow;
    }

    @Override
    @Transactional
    public void deleteOrder(Long id) {
        Order order = em.find(Order.class, id);
        em.remove(order);
    }

    @Override
    @Transactional
    public void deleteAllOrders() {
        Query q = em.createQuery("delete from Order");
        q.executeUpdate();
    }
}
